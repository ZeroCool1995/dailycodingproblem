/*
    Daily Coding Problem #1

    Given a list of numbers and a number k, return whether any two numbers from the list add up to k.
    For example, given [10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17.
    Bonus: Can you do this in one pass?
*/

#include <cstdio>
#include <map>

using namespace std;

int main() {

    int arr[] = {5, 5, 2, 3};
    int k = 10;

    int n = sizeof(arr) / sizeof(arr[0]);
    map<int, int> m;
    
    bool can = false;
    for( int i = 0; i < n; ++i ) {

        m[arr[i]]++;

        int a = arr[i];
        int b = k-arr[i];

        if( a == b ) 
            can |= m[a] >= 2 ;
        else 
            can |= m[a] && m[b];
    }

    puts( can ? "yes" : "no" );

    return 0;
}